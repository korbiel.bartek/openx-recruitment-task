public class Tests {
    public Tests() {
        //tree from the task example
        BinaryTree tree = new BinaryTree();
        tree.root = new Node(5);
        tree.AddNode(tree.root, 3, "left");
        tree.AddNode(tree.root.left, 2, "left");
        tree.AddNode(tree.root.left, 5, "right");
        tree.AddNode(tree.root, 7, "right");
        tree.AddNode(tree.root.right, 1, "left");
        tree.AddNode(tree.root.right, 0, "right");
        tree.AddNode(tree.root.right.right, 2, "left");
        tree.AddNode(tree.root.right.right, 8, "right");
        tree.AddNode(tree.root.right.right.right, 5, "right");

        testLeafsCount(tree, 5);
        testLongestPath(tree, 4);
        testAreEqual(tree, tree, true);

        //second tree
        BinaryTree secondTree = new BinaryTree();
        secondTree.root = new Node(0);
        secondTree.AddNode(secondTree.root, 0, "left");
        secondTree.AddNode(secondTree.root.left, 0, "left");
        secondTree.AddNode(secondTree.root.left.left, 0, "left");
        secondTree.AddNode(secondTree.root, 0, "right");
        secondTree.AddNode(secondTree.root.right, 0, "right");
        secondTree.AddNode(secondTree.root.right.right, 0, "left");

        testLeafsCount(secondTree, 2);
        testLongestPath(secondTree, 3);
        testAreEqual(tree, secondTree, false);
        testAreEqual(secondTree, secondTree, true);
    }

    public static void testLeafsCount(BinaryTree tree, int expected) {
        int result = tree.LeafsCount();
        if (result != expected) {
            System.out.println("Error: testLeafsCount failed.");
        }
    }

    public static void testLongestPath(BinaryTree tree, int expected) {
        int result = tree.LongestPath();
        if (result != expected) {
            System.out.println("Error: testLongestPath failed.");
            System.out.println(result);
        }
    }

    public static void testAreEqual(BinaryTree first, BinaryTree second, Boolean expected) {
        Boolean result = first.isEqual(second);
        if (result != expected) {
            System.out.println("Error: testAreEqual failed.");
        }
        result = second.isEqual(first);
        if (result != expected) {
            System.out.println("Error: testAreEqual failed.");
        }
        result = first.isEqual(first);
        if (result != true) {
            System.out.println("Error: testAreEqual failed.");
        }
        result = second.isEqual(second);
        if (result != true) {
            System.out.println("Error: testAreEqual failed.");
        }
    }
}
