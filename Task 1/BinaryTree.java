class Node {
    int value;
    Node left;
    Node right;
    
    public Node(int value) {
        this.value = value;
        left = null;
        right = null;
    }
}

class BinaryTree {
    Node root;
    
    public BinaryTree() {
        root = null;
    }

    public void AddNode(Node parentNode, int value, String side) {
        if (side == "left") {
            if (parentNode.left == null) {
                Node newNode = new Node(value);
                parentNode.left = newNode;
            }
        } else if(side == "right") {
            if (parentNode.right == null) {
                Node newNode = new Node(value);
                parentNode.right = newNode;
            }
        }
    }

    public void RemoveNode(Node node) {
        node = null;
    }

    //function that counts leafs
    public int LeafsCount() {
        return CountLeafsDFS(root);
    }
    private int CountLeafsDFS(Node current) {
        if (current == null) {
            return 0;
        }
        if (current.left != null || current.right != null) {
            return CountLeafsDFS(current.left) + CountLeafsDFS(current.right);
        }
        return 1;
    }

    public int LongestPath() {
        return LongestPatchDFS(root);
    }
    private int LongestPatchDFS(Node current) {
        if (current == null || (current.left == null && current.right == null)) {
            return 0;
        }
        return 1 + Math.max(LongestPatchDFS(current.left), LongestPatchDFS(current.right));
    }

    public Boolean isEqual(BinaryTree other) {
        return isEqualDFS(root, other.root);
     }
     private Boolean isEqualDFS(Node first, Node second) {
        if (first == null && second == null) {
            return true;
        }
        if (first == null || second == null) {
            return false;
        }
        if (first.value == second.value) {
            if (first.left == second.left && first.right == second.right) {
                return (isEqualDFS(first.left, second.left) && isEqualDFS(first.right, second.right));
            }
            if (first.left == second.right && first.right == second.left) {
                return (isEqualDFS(first.left, second.right) && isEqualDFS(first.right, second.left));
            }
        }
        return false;
     }
}