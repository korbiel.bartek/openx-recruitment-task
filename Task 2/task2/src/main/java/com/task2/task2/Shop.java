package com.task2.task2;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;

public class Shop {
    public List<User> users;
    public List<Product> products;
    public List<Cart> carts;
    public HashMap<String, Double> categories = new HashMap<>(); //product categories

    public void FetchUsers() throws JsonMappingException, JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "https://fakestoreapi.com/users";
        String response = restTemplate.getForObject(apiUrl, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        this.users = objectMapper.readValue(response, new TypeReference<List<User>>(){});
    }

    public void FetchProducts() throws JsonMappingException, JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "https://fakestoreapi.com/products";
        String response = restTemplate.getForObject(apiUrl, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        this.products = objectMapper.readValue(response, new TypeReference<List<Product>>(){});
    }

    public void FetchCarts() throws JsonMappingException, JsonProcessingException {
        /*
         At the time of writing the code, the fakestoreapi.com/carts page was not working, and returned the following error message:
            "status": "error",
            "message": "date format is not correct. it should be in yyyy-mm-dd format"

        Therefore, I used the suggestion provided in https://github.com/keikaavousi/fake-store-api/issues/85, which recommended 
        using the link https://fakestoreapi.com/carts?startdate=2019-12-10&enddate=2020-10-10.
         */
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "https://fakestoreapi.com/carts?startdate=2019-12-10&enddate=2020-10-10";
        String response = restTemplate.getForObject(apiUrl, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        this.carts = objectMapper.readValue(response, new TypeReference<List<Cart>>(){});
    }

    public void CreateProductCategories() throws JsonMappingException, JsonProcessingException {
        categories.clear();
        for (Product product : products) {
            if (categories.containsKey(product.category)) {
                double val = categories.get(product.category);
                val += product.price;
                categories.put(product.category, val);
            } else {
                categories.put(product.category, product.price);
            }
        }
    }

    public void HighestValueCart() throws JsonMappingException, JsonProcessingException {
        double maxValue = 0;
        int resultUserId = 1;
        for (Cart cart: carts) {
            double currentValue = 0;
            for (CartProduct cartProduct: cart.products) {
                double value = ProductValue(cartProduct.productId);
                currentValue += value * cartProduct.quantity;
            }
            if (currentValue > maxValue) {
                maxValue = currentValue;
                resultUserId = cart.userId;
            }
        }
        System.out.print("Cart with the highest value is worth " + maxValue );
        System.out.println(". It's owner is " + UserName(resultUserId));
    }

    public double ProductValue(int id) {
        for (Product product: products) {
            if (product.id == id) {
                return product.price;
            }
        }
        return 0;
    }

    public String UserName(int id) {
        for (User user: users) {
            if (user.id == id) {
                return user.name.firstname + " " + user.name.lastname;
            }
        }
        return "";
    }

    public void FindFurthestClients() throws JsonMappingException, JsonProcessingException {
        double max = 0;
        User resultFirst = users.get(0);
        User resultSec = users.get(0);

        for (int i = 0; i < users.size(); i++) {
            User first = users.get(i);
            for (int j = i + 1; j < users.size(); j++) {
                User second = users.get(j);
                double distance = Math.pow(first.address.geolocation.lat - second.address.geolocation.lat, 2) + Math.pow(first.address.geolocation._long - second.address.geolocation._long, 2);

                if (distance > max) {
                    max = distance;
                    resultFirst = first;
                    resultSec = second;
                }
            }
        }
        System.out.print("Users living furthest away from each other are ");
        System.out.print(resultFirst.name.firstname + " " + resultFirst.name.lastname + " and ");
        System.out.println(resultSec.name.firstname + " " + resultSec.name.lastname);
    }
}