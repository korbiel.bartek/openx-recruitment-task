package com.task2.task2;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    public int id;
    public Address address;
    public Name name;
    public User() {}

    public User(int id, Address address, Name name) {
        this.id = id;
        this.address = address;
        this.name = name;
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Address{
    public Geolocation geolocation;
    public Address() {}

    public Address(Geolocation geolocation) {
        this.geolocation = geolocation;
    }
}

class Geolocation {
    public double lat;
    public double _long;
    public Geolocation(double lat, double _long) {
        this.lat = lat;
        this._long = _long;
    }
    @JsonCreator
    public static Geolocation valueOf(@JsonProperty("lat") double lat, 
                                      @JsonProperty("long") double _long) {
        return new Geolocation(lat, _long);
    }
}

class Name {
    public String firstname;
    public String lastname;
    public Name() {}

    public Name(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
}