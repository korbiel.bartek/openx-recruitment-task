package com.task2.task2;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@SpringBootApplication
public class Task2Application {

	public static void main(String[] args) throws JsonMappingException, JsonProcessingException {
		SpringApplication.run(Task2Application.class, args);
		Shop shop = new Shop();
		shop.FetchCarts();
		shop.FetchProducts();
		shop.FetchUsers();

		shop.CreateProductCategories();
		System.out.println(shop.categories);
		shop.FindFurthestClients();
		shop.HighestValueCart();
		/*
		 Output:
		 {electronics=1994.99, women's clothing=157.72, men's clothing=204.23000000000002, jewelery=883.98}
		 Users living furthest away from each other are john doe and derek powell
		 Cart with the highest value is worth 2578.7. It's owner is john doe
		 */


		//simple test
		shop.users = Arrays.asList(
			new User(1, new Address(new Geolocation(0, 0)), new Name("John", "Rambo")), 
			new User(2, new Address(new Geolocation(90, 90)), new Name("Santa", "Claus")), 
			new User(3, new Address(new Geolocation(-80.3, 50.321)), new Name("Leonardo", "da Vinci")) 
		);
		shop.products = Arrays.asList(
			new Product(1, "Reindeer", "Animals", 50.2), 
			new Product(2, "Red outfit", "Men's clothes", 99.99), 
			new Product(3, "Bazooka", "Guns", 60.54),
			new Product(4, "Grenade", "Guns", 40.23),
			new Product(5, "Paintbrush", "For painting", 8.99),
			new Product(5, "Canvas", "For painting", 6.99)
		);
		shop.carts = Arrays.asList(
			new Cart(1, Arrays.asList(
				new CartProduct(3, 2),
				new CartProduct(4, 10)
			)), 
			new Cart(2, Arrays.asList(
				new CartProduct(1, 7),
				new CartProduct(2, 1)
			)),  
			new Cart(3, Arrays.asList(
				new CartProduct(5, 7),
				new CartProduct(6, 1)
			))  
		);

		shop.CreateProductCategories();
		System.out.println(shop.categories);
		shop.FindFurthestClients();
		shop.HighestValueCart();
		/*
		 Expected output:
		 {Guns=100.77, Animals=50.2, Men's clothes=99.99, For painting=15.98}
		 Users living furthest away from each other are Santa Claus and Leonardo da Vinci
		 Cart with the highest value is worth 523.38. It's owner is John Rambo
		 */
	}

}
