package com.task2.task2;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cart {
    public int userId;
    public List<CartProduct> products;
    public Cart() {}

    public Cart(int userId, List<CartProduct> products) {
        this.userId = userId;
        this.products = products;
    }
}

class CartProduct {
    public int productId;
    public int quantity;
    public CartProduct() {}

    public CartProduct(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }
}